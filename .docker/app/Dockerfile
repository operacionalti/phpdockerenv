FROM php:5.6-fpm-alpine

LABEL maintainer="rodrigo@operacionalti.com"

ARG SYSTEM_APP_DIR

ENV SYSTEM_APP_DIR=${SYSTEM_APP_DIR:-/system/app}
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="${SYSTEM_APP_DIR}/vendor/bin:${PATH}"

WORKDIR ${SYSTEM_APP_DIR}

COPY . ${SYSTEM_APP_DIR}

# Install PHP Extensions
RUN apk add --no-cache libmcrypt-dev \
    && docker-php-ext-install mcrypt

# Install Composer
COPY .docker/app/system/app/scripts/composer-installer.sh ${SYSTEM_APP_DIR}/scripts/composer-installer

RUN chmod +x ${SYSTEM_APP_DIR}/scripts/composer-installer \
    && ${SYSTEM_APP_DIR}/scripts/composer-installer \
    && composer --version

EXPOSE 9000

COPY .docker/app/entrypoint.sh /entrypoint.sh 
RUN chmod +x /entrypoint.sh 

ENTRYPOINT [ "/entrypoint.sh" ]
CMD [ "php-fpm" ]