#!/bin/sh

EXPECTED_HASH=$(curl -s https://composer.github.io/installer.sig)
php -r "copy('https://getcomposer.org/installer', '/tmp/composer-setup.php');"
CURRENT_HASH=$(php -r "echo hash_file('SHA384', '/tmp/composer-setup.php');")

if [ "$EXPECTED_HASH" != "$CURRENT_HASH" ];then
    >&2 echo 'ERROR: Invalid installer hash'
    rm -f /tmp/composer-setup.php
    exit 1
fi

php /tmp/composer-setup.php --quiet --install-dir=/usr/local/bin --filename=composer
RESULT=$?
rm -f /tmp/composer-setup.php
exit $RESULT
