# phpdockerenv

Uma estrutura básica para você começar o seu projeto em um ambiente de containers Docker.

- `db`: Container de banco de dados, atualmente MySQL 5.6.
- `app`: Container de aplicação, atualmente php-fpm 5.6.
- `web`: Container do servidor web, atualmente nginx 1.15.

## Como Usar

```bash
# Clone o projeto para a pasta desejada
git clone git@gitlab.com:operacionalti/phpdockerenv.git projeto

# Reinicie o versionamento git
cd projeto
rm -rf .git
git init

# Atualize o arquivo de variáveis de ambiente de acordo as suas preferências
mv .env-sample .env
vim .env

# Levante os containers
docker-compose up

# Acesse o contaienr php e use o composer para instalar as dependências do seu projeto
docker exec -it projeto_phpfpm_1 sh
composer require laravel/laravel
```

## Por que não usar o PHP e o composer da minha máquina

Por que as versões instaladas na sua máquina geralmente são diferentes das versões utilizadas no container. Quando você usa os comandos `php` e `composer` do container de aplicação - `app` - você tem a garantia de estar usando as mesmas versões utilizadas pelos outros desenvolvedores e também as mesmas versões utilizadas em produção.

Se você possui o `php` e o `composer` instalados no seu computador, você pode verificar a diferença de versões executando o comando abaixo no terminal do seu próprio sistema operacional e comparar a saída com a execução dele dentro do container de aplicações:

```bash
# No seu próprio sistema
composer show --platform

# Dentro do container de aplicação
docker exec -it phpdockerenv_app_1 sh
composer show --platform
```
