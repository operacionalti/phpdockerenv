<?php

/*
|--------------------------------------------------------------------------
| Registra o Auto Loader
|--------------------------------------------------------------------------
|
| Utiliza o mecanismo de autoload disponibilizado pelo composer.
| Classes PSR-4 de pacotes instalados pelo composer ou registradas
| diretamente no composer.json serão carregadas automaticamente
| na aplicação quando forem necessárias. Não é mais necessário utilizar
| includes ou requires espalhados em diversos arquivos do projeto ;-)
|
*/

require_once __DIR__.'/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Inicializa a aplicação
|--------------------------------------------------------------------------
|
| Todas as requisições que não forem para arquivos estáticos são 
| encaminhadas para este arquivo - index.php - através das configurações
| do servidor web. Este arquivo inclui o bootstrap/app.php, responsável
| por inicializar a aplicação.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Executa a Aplicação
|--------------------------------------------------------------------------
|
| Uma vez inicializada, podemos dar continuidade no fluxo da requisição.
| Há várias formas de despachar a requisição, a seguir esamos apenas
| verificando se o arquivo requisitado existe e enviando o resultado
| de sua execução diretamente como resposta da requisição.
|
*/

echo "Alo mundo, from: ".__FILE__."!";