<?php
/*
|--------------------------------------------------------------------------
| Inicializa a aplicação
|--------------------------------------------------------------------------
|
| Carrega variáveis de ambiente a partir do arquivo .env
| As variáveis ficarão disponíveis através da função getenv('VAR_NAME')
| Também é configurado um mecanismo de log básico através do monolog
| As chamadas ficarão acessíveis através da variável global $log.
| Os logs serão armazenados em storage/appname.log
|
*/

use Dotenv\Dotenv;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

# Carraga as variáveis de ambiente a partir do arquivo ".env"
$dotenv = Dotenv::create('../');
$dotenv->load();

# Variáveis carregadas do .env
# Necessárias para criar o arquivo de log
$APP_NAME = getenv('APP_NAME');
$BASE_DIR = getenv('BASE_DIR');

# Corrige mensagem de alerta quando TIMEZONE não está definido no php.ini
date_default_timezone_set(getenv('TIMEZONE'));

#  Cria arquivo de log para mensagens de qualquer nível de criticidade
$log = new Logger($APP_NAME);
$log->pushHandler(new StreamHandler("../storage/${APP_NAME}.log", Logger::DEBUG));
